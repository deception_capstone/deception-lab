# Deception-Lab
Deception Technology lab for UW Capstone


## Getting started

ansible-playbooks contains AnsibleCommands used to install docker and OpenCanary honetypots

OpenCanary config files are in the folder configs --> confv1 / opencanary.conf
<br>--> confv2 / opencanary.conf									 
<br>--> confv3 / opencanary.conf

If you wanted to run more canaries or different services/ports you can modify and add more config files here:
https://gitlab.com/deception_capstone/deception-lab/-/tree/main/ansible-playbooks/configs

For email reporting make sure to change the email and APPPASSWD and subject line in each opencanary.conf

You can have as many config files and opencanary honeypots as you want with minor edits to the scripts.
Log files are in the path {{ HOME_DIR }}opencanary/logs each with a seperate log name such as opencanaryv1.log
These can be scraped with filebeat etc.

The variables you need to change are here:
https://gitlab.com/deception_capstone/deception-lab/-/blob/main/ansible-playbooks/tasks/opencanary.yml

All the variables you need to edit are located in tasks --> opencanary.yml at the top of the file.
These commands are passed to the install scripts as arguments.
The Ansible script will ask you for a sudo password in order to start docker containers.
There are a few changes you could make in ansible-playbooks/startup.sh if you wish to add additional honeypots

This is scalable, so you could easily modify the script to run multiple subnets/VLANs in the setup script:
https://gitlab.com/deception_capstone/deception-lab/-/blob/main/ansible-playbooks/setup.sh

You could scale up the number of honeypots in the start up script:
https://gitlab.com/deception_capstone/deception-lab/-/blob/main/ansible-playbooks/startup.sh


A video of how to use the Ansible script can be found here:
https://drive.google.com/file/d/1gPyVe8hpNWMdc_WMwj1Rxm0PStYoTOqS/view?usp=sharing


If your honeypots are placed in a subnet that is not routable to the internet on the same interface you may make use of the ocemail.sh script in log_emailer/ocemail.sh
In this script the inotifywait command watches the entire log file directory for modification and when triggered outputs a file rather than standard output which is parsed and edited down with sed to produce just the filename of the log that changed.
that logfile is then passed as an argument to the ocmail script which tails the last 2 lines to be emailed out. This script is only needed if the subnet the opencanary honeypots are placed on is not routeable as they have internal email alerting.

Another script that watches a list of files for modification and triggers a canarytoken is here
log_emailer/filewatchscript.sh
https://gitlab.com/deception_capstone/deception-lab/-/blob/main/log_emailer/filewatchscript.sh

ansible-playboks for installing lampstack and filebeats are under Nix


## Project status
This is a capstone project 2022
