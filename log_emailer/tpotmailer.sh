#!/bin/bash
#this shell script will watch a log directory for modications and email the last 2 lines of the log file each time a log file is updated
#prerequisites
#apt-get install inotify-tools swaks screen
#see bottom COMMENT for required detacthed screen 
#
#this line accepts a filename as an argument from the script running in the detached screen
TEXT_VAR=$1
#this line will tail the last 2 lines into a variable
READ=$(tail -n 2 $TEXT_VAR)
#this command will email the last 2 lines of the log file
swaks --to trustedcanaries@gmail.com --from "trustedcanaries@gmail.com" --server smtp.gmail.com:587 -tls --auth-user trustedcanaries@gmail.com --auth-password 'APP_PASSWD' --header "Subject: T Pot Honeypot Alert from whatever network" --body "$READ"
#EOF ocemail.sh
#
#
#The following commands are run in a detached screen in order to watch the directoy with the logs
#the inotifywait command watches the entire log file directory for modification and when triggered outputs a file rather than standard output which is parsed and edited down with sed to produce just the filename of the log file that changed
#that logfile is then tailed and printed to the screen before the filename is passed as an argument to the above ocmail script to be emailed out.
#the entire log file could also be attached with the swaks flag --attach 
#
: << 'COMMENT'
screen -R watch1
#this can be run in a screen or as a script
##!/bin/bash -i
#watches a folder and outputs sdout to a file NOTIFYFILE
while inotifywait -e modify /data/cowrie/log/cowrie.json /data/dionaea/log/dionaea.json -o NOTIFYFILE; do
#sed parses and trims the stdout in NOTIFYFILE down to the filename that was modified only
cat NOTIFYFILE | sed -e 's/\<MODIFY\>//g' > OUTFILEEDIT
#create a logfile of all stdout for a list of all the modified files
cat NOTIFYFILE >> NOTIFYFILE.log
#delete the file
rm NOTIFYFILE
#read the edited down trimmed name of accessed file to a variable
READ1=$(cat OUTFILEEDIT)
echo $READ
#get the last 5 lines from the honeypot logfile
PRINTOUT=$(tail -n 2 $READ)
echo $PRINTOUT
#execute emailing script
/home/itadmin/opencanary/ocemail.sh $READ
done
#Ctrl-a Ctrl-d
COMMENT
