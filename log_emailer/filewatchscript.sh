#!/bin/bash -i
#this shell script will watch several files for modications and trigger a canarytoken when they are modified
#prerequisites
#apt-get install inotify-tools screen
#suggested folders to watch /etc/profile ~/.bash_profile ~/.bash_login ~/.profile. /home/itadmin/.bashrc /etc/bash.bashrc /etc/profile.d/ /etc/resolv.conf /etc/hosts /etc/passwd /etc/group /etc/apt/sources.list

while inotifywait -e modify  /etc/profile /home/itadmin/.bashrc /etc/bash.bashrc /etc/profile.d/ /etc/resolv.conf /etc/hosts  /etc/passwd /etc/group /etc/apt/sources.list -o NOTIFYFILE; do
cat NOTIFYFILE | sed 's/.*\///' | sed -e 's/\<MODIFY\>//g' | sed "s/^[ \t]*//" > OUTFILEEDIT
NOTIFYFILE >> NOTIFYFILE.log
rm NOTIFYFILE
READ=$(cat OUTFILEEDIT)
echo $READ

#add your canarytoken url here
#curl https://canarytokens.org/generate#
#you can pass a small message into the canarytoken as a  User Agent string such as the log file that was accessed
#https://blog.thinkst.com/2020/05/canarytokens-token-anything-anywhere.html
curl -s -A "$READ" http://canarytokens.com/tokenurl
#you could have several if statements read $READ and trigger a differnt canarytokens URL based off of which file was modified
# The last file that was modified will be listed in the file OUTFILEEDIT
#This is >> appended to a logfile NOTIFYFILE.log with a list of all watched files that were modified

done

