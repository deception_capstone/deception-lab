#!/bin/bash -i
#script for starting container
HOME_DIR=$1
SUBNET=$2
GATE_WAY=$3
INTFACE=$4

cd "$HOME_DIR"opencanary
#build docker
docker build -t opencanary -f Dockerfile.stable .
#check images
docker images
#future adventures in ipvlan networking
#docker network  create -d ipvlan --subnet=192.168.0.0/24 --gateway=192.168.0.1 -o ipvlan_mode=l2 -o parent=eth0 ipcanarynet
docker network  create -d ipvlan --subnet=$SUBNET --gateway=$GATE_WAY -o ipvlan_mode=l2 -o parent=$INTFACE ipcanarynet
#sudo docker network  create  -d ipvlan \
#	--subnet=192.168.0.0/24 \
#	--gateway=192.168.0.1  \
#	 -o ipvlan_mode=l2 \
#	 -o parent=eth0 ipcanarynet

touch "$HOME_DIR"opencanary/foobar
ping -c 20 127.0.0.1
echo "fin"
rm "$HOME_DIR"opencanary/foobar
