#!/bin/bash -i
#script for starting container
dir=$(pwd)
echo "$dir"
HOME_DIR=$1
BECOME_PASS=$2
IPADDR1=$3
IPADDR2=$4
IPADDR3=$5
#if you pass more than 9 arguments to this script you will need to use the following format beyond $9: IPADDR10=${10}
echo homedir $HOME_DIR

cd $HOME_DIR
cd opencanary

cp data/twistd.pid confv1/twistd.pid
cp data/'twistd 2.pid' confv1/'twistd 2.pid'

cp data/twistd.pid confv2/twistd.pid
cp data/'twistd 2.pid' confv2/'twistd 2.pid'

cp data/twistd.pid confv3/twistd.pid
cp data/'twistd 2.pid' confv3/'twistd 2.pid'

#run opencanaryv1 #restart=no to prevent infinite loops if not interacticve script
#start opencanaryv1 in screen
screen -A -m -d -S screen1 sh -c "sudo docker run -it --name opencanaryv1 --net=ipcanarynet --ip $IPADDR1 -p 80:80 -p 21:21 -p 22:22 -p 69:69 -v "$HOME_DIR"opencanary/logs:/usr/share/logs -v "$HOME_DIR"opencanary/confv1:/root opencanary /bin/bash && ping -c 4 127.0.0.1"

#sudo passwd here for user
screen -S screen1 -X stuff "$BECOME_PASS
"
ping -c 4 127.0.0.1
screen -S screen1 -X stuff 'opencanaryd --start;
'

#or do sudo docker exec -it opencanaryv1 opencanaryd --start;

#run opencanaryv2 container
screen -A -m -d -S screen2 sh -c "sudo docker run -it --name opencanaryv2 --net=ipcanarynet --ip $IPADDR2 -p 3306:3306 -p 22:22 -v "$HOME_DIR"opencanary/logs:/usr/share/logs -v "$HOME_DIR"opencanary/confv2:/root opencanary /bin/bash && ping -c 4 127.0.0.1"

#sudo passwd here for user
screen -S screen2 -X stuff "$BECOME_PASS
"

ping -c 4 127.0.0.1
screen -S screen2 -X stuff 'opencanaryd --start;
'

#run opencanaryv3
screen -A -m -d -S screen3 sh -c "sudo docker run -it --name opencanaryv3 --net=ipcanarynet --ip $IPADDR3 -p 5000:5000 -p 22:22 -v "$HOME_DIR"opencanary/logs:/usr/share/logs -v "$HOME_DIR"opencanary/confv3:/root opencanary /bin/bash && ping -c 4 127.0.0.1"

#sudo passwd here for user
screen -S screen3 -X stuff "$BECOME_PASS
"

ping -c 4 127.0.0.1
screen -S screen3 -X stuff 'opencanaryd --start;
'


ping -c 20 127.0.0.1
echo "fin"
rm -rf $dir/startup.sh
ping -c 60 127.0.0.1
